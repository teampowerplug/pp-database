﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

public class PowerPlugNode
{
    uint DeviceID;
    uint MemberID;
    string DeviceName;
    string Description;
    string fileName_Image;
    List<string> TriggerList;
    uint HardwareID;
}

public class PowerPlugUser
{
    uint UserID;
    string UserNam;
    string password;
    List<string> previousPasswords;
    DateTime passwordSetDate;
    List<PowerPlugNode> DeviceList;
}

namespace SqlServer_Connection
{
 

    static class DataParser
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }


    }
}
